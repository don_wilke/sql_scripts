use master;
go 
sp_configure 'show advanced options',1;
go 
reconfigure with override;
go 
sp_configure 'Database Mail XPs',1;
--go 
--sp_configure 'SQL Mail XPs',0;
go 
reconfigure;
go 
USE msdb
GO
-------------------------------------------------------------------------------------------------- 
-- BEGIN @mailprofile Settings
-------------------------------------------------------------------------------------------------- 
  DECLARE @server as nvarchar(128) =  Cast(SERVERPROPERTY('servername') as nvarchar(100))
  DECLARE @servername as nvarchar(128) =  replace(@server,'\','')
  DECLARE @descriptiontxt as nvarchar(300) = @servername +												N' Sql Instance Alerts'  
  DECLARE @email_addresstxt as nvarchar(300) = @servername +	N'@edens.com'  
  DECLARE @mailProfile as nvarchar(100) =																N'Mail'
  DECLARE @mailAccount as nvarchar(100) =																N'SQL Mail'
  DECLARE @mailServer_name as nvarchar(100) =															N'outlook.edens.com'
  DECLARE @mailOperator as nvarchar(100) =																N'DBA'
  DECLARE @mailOperatorEmail as nvarchar(100) =															N'DBAdmins@edens.com'
  
  IF NOT EXISTS(SELECT * FROM msdb.dbo.sysmail_profile WHERE  name = @mailProfile)  
  BEGIN 
    --CREATE Profile @mailProfile 
    EXECUTE msdb.dbo.sysmail_add_profile_sp 
      @profile_name = @mailProfile, 
      @description  = ''; 
  END --IF EXISTS profile 
  
  IF NOT EXISTS(SELECT * FROM msdb.dbo.sysmail_account WHERE  name = @mailAccount) 
  BEGIN 
    --CREATE Account @mailAccount 
    EXECUTE msdb.dbo.sysmail_add_account_sp 
    @account_name            = @mailAccount, 
    @email_address           = @email_addresstxt, 
    @display_name            = @servername, 
    @replyto_address         = '', 
    @description             = @descriptiontxt, 
    @mailserver_name         = @mailServer_name, 
    @mailserver_type         = 'SMTP', 
    @port                    = '25', 
    @username                =  NULL , 
    @password                =  NULL ,  
    @use_default_credentials =  1 , 
    @enable_ssl              =  0 ; 
  END --IF EXISTS  account 
   
IF NOT EXISTS(SELECT * 
              FROM msdb.dbo.sysmail_profileaccount pa 
                INNER JOIN msdb.dbo.sysmail_profile p ON pa.profile_id = p.profile_id 
                INNER JOIN msdb.dbo.sysmail_account a ON pa.account_id = a.account_id   
              WHERE p.name = @mailProfile 
                AND a.name = @mailAccount)  
  BEGIN 
    -- Associate Account @mailAccount to Profile @mailProfile
    EXECUTE msdb.dbo.sysmail_add_profileaccount_sp 
      @profile_name = @mailProfile, 
      @account_name = @mailAccount, 
      @sequence_number = 1 ; 
  END  
--IF EXISTS associate accounts to profiles 
--------------------------------------------------------------------------------------------------- 
-- Drop Settings For @mailProfile
-------------------------------------------------------------------------------------------------- 
/* 
IF EXISTS(SELECT * 
            FROM msdb.dbo.sysmail_profileaccount pa 
              INNER JOIN msdb.dbo.sysmail_profile p ON pa.profile_id = p.profile_id 
              INNER JOIN msdb.dbo.sysmail_account a ON pa.account_id = a.account_id   
            WHERE p.name = @mailProfile 
              AND a.name = @mailAccount) 
  BEGIN 
    EXECUTE msdb.dbo.sysmail_delete_profileaccount_sp @profile_name = @mailProfile, @account_name = @mailAccount 
  END  
IF EXISTS(SELECT * FROM msdb.dbo.sysmail_account WHERE  name = @mailAccount) 
  BEGIN 
    EXECUTE msdb.dbo.sysmail_delete_account_sp @account_name = @mailAccount 
  END 
IF EXISTS(SELECT * FROM msdb.dbo.sysmail_profile WHERE  name = @mailProfile)  
  BEGIN 
    EXECUTE msdb.dbo.sysmail_delete_profile_sp @profile_name = @mailProfile 
  END 
*/ 

--Create @mailOperator Operator for notifications

--EXEC msdb.dbo.sp_delete_operator @name = @mailOperator

EXEC msdb.dbo.sp_add_operator @name = @mailOperator, 
		@enabled=1, 
		@weekday_pager_start_time=90000, 
		@weekday_pager_end_time=180000, 
		@saturday_pager_start_time=90000, 
		@saturday_pager_end_time=180000, 
		@sunday_pager_start_time=90000, 
		@sunday_pager_end_time=180000, 
		@pager_days=0, 
		@email_address= @mailOperatorEmail, 
		@category_name=N'[Uncategorized]';

EXEC master.dbo.sp_MSsetalertinfo @failsafeoperator= @mailOperator, 
		@notificationmethod=1;

EXEC master.dbo.sp_MSsetalertinfo @failsafeoperator= @mailOperator;

EXEC msdb.dbo.sp_set_sqlagent_properties @email_save_in_sent_folder=1, 
		@databasemail_profile = @mailProfile, 
		@use_databasemail=1;
