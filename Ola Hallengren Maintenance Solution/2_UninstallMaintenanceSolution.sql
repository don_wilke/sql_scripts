-- File Name    :       Uninstall Ola Hallengren Maintenance System.sql
-- Author       :       Graham Okely B App Sc
-- Email        :       Graham.DBA@gmail.com
-- Blog         :       http://sqlgokely.blogspot.com.au/
-- Disclaimer   :       Use at your own risk of course! It may help someone.
-- Reference    :       http://ola.hallengren.com/


/*
==[ Modifications ]=========================================================
DD/MM/YYYY <WHO>        : [Description of changes]
9/7/2012 GJO            : Initial version
10/7/2012 GJO           : Altered function drop process

8/10/2012 GJO           : NOTE: This is for a clean uninstall for th3e purpose of
: reinstallation! THe system is a good product.

============================================================================
*/


--------------------------------------
-- Select Database
use master
go

Declare @Select_Server          varchar(1024)
Declare @Current_Server         varchar(1024)
Declare @Choice                 int

----------------------------------------
-- Ensure you have the correct server
----------------------------------------
-- Specify servername
Set     @Select_Server          =       @@SERVERNAME
Select  @Current_Server         =       @@servername
SELECT @Select_Server
SELECT @Current_Server
----------------------------------------
-- Select level of clean up
Set      @Choice                =       0
-- 0 all
-- 1 tables
-- 2 Drop stored procedures
-- 3 Drop function
-- 4 Drop jobs
-- 5 Drop purge jobs

if ( @Select_Server = @Current_Server)
BEGIN
Print 'Uninstall Ola Maintenance System from ' + @Select_Server
Print 'At level ' + cast (@Choice as varchar(1))
Print getdate()

----------------------------------------
-- Drop tables
if (@Choice = 1 or @Choice=0 )
BEGIN
IF OBJECT_ID (N'dbo.CommandLog','U') IS NOT NULL and OBJECT_ID (N'dbo.CommandLog_Bak','U') IS  NULL
BEGIN
-- Backup log
select * into [dbo].[CommandLog_Bak] from [dbo].[CommandLog]
-- log action
insert into [dbo].[CommandLog_Bak] (Command,CommandType,StartTime) values ('Uninstall all objects','CLEAN',getdate())
drop table [dbo].[CommandLog]
END
END
----------------------------------------
-- Drop stored procedures
if (@Choice = 2 or @Choice=0 )
BEGIN
IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'CommandExecute')
                                        drop procedure [dbo].[CommandExecute]
IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'DatabaseBackup')
                                        drop procedure [dbo].[DatabaseBackup]
IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'DatabaseIntegrityCheck')
                                        drop procedure [dbo].[DatabaseIntegrityCheck]
IF EXISTS (SELECT * FROM sys.objects WHERE type = 'P' AND name = 'IndexOptimize')
                                        drop procedure [dbo].[IndexOptimize]
END

----------------------------------------
-- Drop functions
if (@Choice = 3 or @Choice=0 )
BEGIN
IF OBJECT_ID('[dbo].[DatabaseSelect]') IS NOT NULL
DROP FUNCTION [dbo].[DatabaseSelect]
END

----------------------------------------
-- Drop jobs
if (@Choice = 4 or @Choice=0 )
BEGIN
IF EXISTS (SELECT job_id FROM msdb.dbo.sysjobs_view WHERE name = N'CommandLog Cleanup')
BEGIN
EXEC msdb.dbo.sp_delete_job @job_name=N'CommandLog Cleanup', @delete_unused_schedule=1
END
IF EXISTS (SELECT job_id FROM msdb.dbo.sysjobs_view WHERE name = N'DatabaseBackup - SYSTEM_DATABASES - FULL')
BEGIN
EXEC msdb.dbo.sp_delete_job @job_name=N'DatabaseBackup - SYSTEM_DATABASES - FULL', @delete_unused_schedule=1
END
IF EXISTS (SELECT job_id FROM msdb.dbo.sysjobs_view WHERE name = N'DatabaseIntegrityCheck - USER_DATABASES Lite')
BEGIN
EXEC msdb.dbo.sp_delete_job @job_name=N'DatabaseIntegrityCheck - USER_DATABASES Lite', @delete_unused_schedule=1
END
IF EXISTS (SELECT job_id FROM msdb.dbo.sysjobs_view WHERE name = N'DatabaseBackup - USER_DATABASES - DIFF')
BEGIN
EXEC msdb.dbo.sp_delete_job @job_name=N'DatabaseBackup - USER_DATABASES - DIFF', @delete_unused_schedule=1
END
IF EXISTS (SELECT job_id FROM msdb.dbo.sysjobs_view WHERE name = N'DatabaseBackup - USER_DATABASES - FULL')
BEGIN
EXEC msdb.dbo.sp_delete_job @job_name=N'DatabaseBackup - USER_DATABASES - FULL', @delete_unused_schedule=1
END
IF EXISTS (SELECT job_id FROM msdb.dbo.sysjobs_view WHERE name = N'DatabaseBackup - USER_DATABASES - LOG')
BEGIN
EXEC msdb.dbo.sp_delete_job @job_name=N'DatabaseBackup - USER_DATABASES - LOG', @delete_unused_schedule=1                                               
END
IF EXISTS (SELECT job_id FROM msdb.dbo.sysjobs_view WHERE name = N'DatabaseIntegrityCheck - SYSTEM_DATABASES')
BEGIN
EXEC msdb.dbo.sp_delete_job @job_name=N'DatabaseIntegrityCheck - SYSTEM_DATABASES', @delete_unused_schedule=1  
END
IF EXISTS (SELECT job_id FROM msdb.dbo.sysjobs_view WHERE name = N'DatabaseIntegrityCheck - USER_DATABASES')
BEGIN
EXEC msdb.dbo.sp_delete_job @job_name=N'DatabaseIntegrityCheck - USER_DATABASES', @delete_unused_schedule=1    
END
IF EXISTS (SELECT job_id FROM msdb.dbo.sysjobs_view WHERE name = N'IndexOptimise')
BEGIN
EXEC msdb.dbo.sp_delete_job @job_name=N'IndexOptimise', @delete_unused_schedule=1      
END
IF EXISTS (SELECT job_id FROM msdb.dbo.sysjobs_view WHERE name = N'IndexOptimize - USER_DATABASES')
BEGIN
EXEC msdb.dbo.sp_delete_job @job_name=N'IndexOptimize - USER_DATABASES', @delete_unused_schedule=1     
END
IF EXISTS (SELECT job_id FROM msdb.dbo.sysjobs_view WHERE name = N'Output File Cleanup')
BEGIN
EXEC msdb.dbo.sp_delete_job @job_name=N'Output File Cleanup', @delete_unused_schedule=1
END
END

----------------------------------------
-- Drop purge jobs
if (@Choice = 5 or @Choice=0 )
BEGIN
IF EXISTS (SELECT job_id FROM msdb.dbo.sysjobs_view WHERE name = N'sp_delete_backuphistory')
        EXEC msdb.dbo.sp_delete_job @job_name=N'sp_delete_backuphistory', @delete_unused_schedule=1    
IF EXISTS (SELECT job_id FROM msdb.dbo.sysjobs_view WHERE name = N'sp_purge_jobhistory')
        EXEC msdb.dbo.sp_delete_job @job_name=N'sp_purge_jobhistory', @delete_unused_schedule=1
END
END