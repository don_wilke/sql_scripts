SELECT 'alter database [' + NAME + '] Set Recovery Simple' AS sqlstatement
FROM master.sys.databases
WHERE NAME IN ('master', 'model', 'msdb')
	AND state_desc = 'online'

UNION ALL

SELECT 'alter database [' + NAME + '] Set Recovery Full' AS sqlstatement
FROM master.sys.databases
WHERE NAME NOT IN ('master', 'tempdb', 'model', 'msdb')
	AND state_desc = 'online'
	;