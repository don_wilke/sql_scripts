USE master;

SELECT name, physical_name AS CurrentLocation, state_desc, reverse(left(reverse(physical_name),Charindex('\',Reverse(physical_name))-1)) AS FlName
FROM sys.master_files
WHERE database_id = DB_ID(N'database_name_here') AND 
type_desc = N'LOG'
/**
ALTER DATABASE database_name_here SET OFFLINE with rollback immediate;
**/
/**
-- Physically move the file to a new location.
-- In the following statement, modify the path specified in FILENAME to
-- the new location of the file on your server.
ALTER DATABASE database_name_here 
    MODIFY FILE ( NAME = mscrm_log, 
                  FILENAME = 'H:\SQL Server Log Files\database_name_here.LDF');
**/
/**
ALTER DATABASE database_name_here SET ONLINE with rollback immediate;
**/
--Verify the new location.
SELECT name, physical_name AS CurrentLocation, state_desc
FROM sys.master_files
WHERE database_id = DB_ID(N'database_name_here')
    AND type_desc = N'LOG';

-- Return anything left on the C:
With CTE AS
(
SELECT name, physical_name AS CurrentLocation, state_desc, reverse(left(reverse(physical_name),Charindex('\',Reverse(physical_name))-1)) AS FlName
FROM sys.master_files
WHERE type_desc = N'LOG'
)
SELECT * FROM CTE
WHERE CurrentLocation like N'C:\%';