SET NOCOUNT ON

SELECT 
   'ALTER TABLE [' 
   + s.[name] 
   + '].[' 
   + o.[name] 
   + '] REBUILD WITH (DATA_COMPRESSION=PAGE);'
FROM sys.objects AS o WITH (NOLOCK)
INNER JOIN sys.indexes AS i WITH (NOLOCK)
   ON o.[object_id] = i.[object_id]
INNER JOIN sys.schemas AS s WITH (NOLOCK)
   ON o.[schema_id] = s.[schema_id]
INNER JOIN sys.dm_db_partition_stats AS ps WITH (NOLOCK)
   ON i.[object_id] = ps.[object_id]
AND ps.[index_id] = i.[index_id]
WHERE o.[type] = 'U'
--AND s.Name LIKE '%%'   -- filter by table name
--AND o.Name LIKE '%%'   -- filter by schema name

SELECT 
   'ALTER INDEX [' 
   + i.[name] 
   + '] ON [' 
   + s.[name] 
   + '].[' 
   + o.[name] 
   + '] REBUILD WITH (DATA_COMPRESSION=PAGE);'
FROM sys.objects AS o WITH (NOLOCK)
INNER JOIN sys.indexes AS i WITH (NOLOCK)
   ON o.[object_id] = i.[object_id]
INNER JOIN sys.schemas s WITH (NOLOCK)
   ON o.[schema_id] = s.[schema_id]
INNER JOIN sys.dm_db_partition_stats AS ps WITH (NOLOCK)
   ON i.[object_id] = ps.[object_id]
AND ps.[index_id] = i.[index_id]
WHERE o.type = 'U' 
AND i.[index_id] >0
--AND i.Name LIKE '%%'   -- filter by index name
--AND o.Name LIKE '%%'   -- filter by table name
ORDER BY ps.[reserved_page_count]

--other stuff
/**
ALTER DATABASE [Las2.0] SET RECOVERY SIMPLE;
GO
USE [Las2.0]
GO
DBCC SHRINKDATABASE(N'Las2.0' );
GO
ALTER DATABASE [Las2.0] SET RECOVERY FULL;
**/